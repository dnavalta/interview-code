""" TITLE: navalta_interview.py
    AUTHOR: Dalton Navalta
    DATE: 15 August, 2018
    
    Implement the functions for the Python challenge.
    Answers items 1, 2, 3, & 5 of the Python challenge
"""

import random  # https://docs.python.org/3.6/library/random.html
import sqlite3  # https://docs.python.org/3.6/library/sqlite3.html
import string  # https://docs.python.org/3.6/library/string.html
# Added by Dalton Navalta - More secure password generation
from secrets import choice  # https://docs.python.org/3.6/library/secrets.html
# Added by Dalton Navalta - Easy password complexity identification
import re
# Added by Dalton Navalta - Accessing https://randomuser.me/api/
from urllib.request import urlopen
import json


def generate_password_helper(length: int, alphabet: list) -> str:
    """Generate a random password with given length and alphabet

    Helper function for generate_password

    param length: number of characters
    param alphabet: list of characters to be used for password creation
    returns: generated password
    """
    # Choose `length` characters at random from the alphabet
    return ''.join(choice(alphabet) for _ in range(length))


def generate_password(length: int, complexity: int) -> str:
    """Generate a random password with given length and complexity

    Complexity levels:
        Complexity == 1: return a password with only lowercase chars
        Complexity ==  2: Previous level plus at least 1 digit
        Complexity ==  3: Previous levels plus at least 1 uppercase char
        Complexity ==  4: Previous levels plus at least 1 punctuation char

    :param length: number of characters
    :param complexity: complexity level
    :returns: generated password
    """
    # Create alphabet according to complexity requirements
    if complexity == 1:
        alphabet = string.ascii_lowercase
        # No chance of accidentally returning a lower complexity, so we can
        #   just use the first password chosen
        return generate_password_helper(length, alphabet)
    elif complexity == 2:
        alphabet = string.ascii_lowercase + string.digits
    elif complexity == 3:
        alphabet = string.ascii_letters + string.digits
    elif complexity == 4:
        alphabet = string.ascii_letters + string.digits + string.punctuation

    # Create a password using helper function
    password = generate_password_helper(length, alphabet)

    # Randomly chosen characters may not fulfill the requirements for the given
    #   complexity, so we must repeatedly construct passwords until
    #   check_password_level returns our desired complexity

    # Case len >= 8 and complexity 1 or 2: we must allow for the exceptions in
    #   check_password_level which result in higher complexities for longer
    #   passwords
    if length >= 8 and complexity in [1,2]:
        while check_password_level(password) != complexity+1:
            password = generate_password_helper(length, alphabet)
    # Default: all other cases (len < 8 or complexity is 3 or 4
    else:
        while check_password_level(password) != complexity:
            password = generate_password_helper(length, alphabet)

    # Return satisfactory password
    return password


def check_password_level(password: str) -> int:
    """Return the password complexity level for a given password

    Complexity levels:
        Return complexity 1: If password has only lowercase chars
        Return complexity 2: Previous level condition and at least 1 digit
        Return complexity 3: Previous levels condition and at least 1 uppercase char
        Return complexity 4: Previous levels condition and at least 1 punctuation

    Complexity level exceptions (override previous results):
        Return complexity 2: password has length >= 8 chars and only lowercase chars
        Return complexity 3: password has length >= 8 chars and only lowercase and digits

    :param password: password
    :returns: complexity level
    """
    # Get password length
    length = len(password)
    # Regular expressions
    # Return complexity 1: If password has only lowercase chars and has length
    #   under 8
    complexity_1 = '[a-z]{1,7}$'
    # Return complexity 2: only lowercase chars, length under 8,
    #   and at least 1 digit
    # Exception: password has length >= 8 and only lowercase chars
    complexity_2 = '[a-z0-9]{1,7}$|[a-z]{8}[a-z]*$'
    # Return complexity 3: lowercase chars and digits with at least one
    #   uppercase char
    # Exception: password has length >= 8 chars and only lowercase and digits
    complexity_3 = '[a-z0-9]{8}[a-z0-9]*$|[a-zA-Z0-9]+$'
    # Return complexity 4: Previous levels condition and at least 1 punctuation
    complexity_4 = '[\S]+$'

    # Check password against complexities
    if re.match(complexity_1, password):
        return 1
    elif re.match(complexity_2, password):
        return 2
    elif re.match(complexity_3, password):
        return 3
    elif re.match(complexity_4, password):
        return 4
    # Return -1: Used to check that whitespace characters are not accepted
    else:
        return -1


def create_user(db_path: str, password: str='') -> None: 
    """Retrieve a random user from https://randomuser.me/api/
    and persist the user (full name and email) into the given SQLite db

    :param db_path: path of the SQLite db file
            (to do: sqlite3.connect(db_path))
    :param password: OPTIONAL password associated with user
    :return: None
    """
    # Set user_source url and name_keys
    user_source = 'https://randomuser.me/api/'
    name_keys = ['title', 'first', 'last']

    # Open connection to database and get cursor
    db_conn = sqlite3.connect(db_path)
    db_cursor = db_conn.cursor()

    # Retrieve and format random user data
    with urlopen(user_source) as new_user:
        # Get and isolate user data
        data = json.loads(new_user.read())
        data = data['results'][0]
        # Get names and concatenate into full name
        full_name = [ data['name'][key] for key in name_keys ]
        full_name = ' '.join(full_name)
        # Get email
        email = data['email']

    # Construct query starting with INSERT call
    query = 'INSERT INTO users(name, email'
    # Password field of INSERT call if password parameter passed
    if password:
        query += ', password'
    # Required values for INSERT to users
    query += ') VALUES(' \
            + repr(full_name) + ', ' \
            + repr(email)
    # Password value if password parameter passed
    if password:
        query += ', ' + repr(password)
    query += ')'
    # Execute query
    db_cursor.execute(query)

    # Commit and close
    db_conn.commit()
    db_cursor.close()
    db_conn.close()


