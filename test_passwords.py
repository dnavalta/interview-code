""" TITLE: test_passwords.py
    AUTHOR: Dalton Navalta
    DATE: 15 August, 2018
    
    Test the functions in navalta_interview from interview-code repository.
    Answers item 4 in Python challenge.
"""

from navalta_interview import check_password_level, generate_password

# A function for testing boolean statements so that I don't have to write too
#   many ifs
def test_condition(condition: bool, message: str) -> None:
    if condition:
        print(message+':\tsuccess')
    else:
        print(message+':\tfailed')


if __name__ == '__main__':
    print('Hard coded passwords')
    test_condition(check_password_level('abdicat') == 1, 'Password level 1')
    test_condition(check_password_level('abdicate') == 2,
            'Password level 2 (long)')
    test_condition(check_password_level('4bdicat') == 2,
            'Password level 2 (short)')
    test_condition(check_password_level('4bdicate') == 3,
            'Password level 3 (long)')
    test_condition(check_password_level('4bDicat') == 3,
            'Password level 3 (short)')
    test_condition(check_password_level('4bDicate!') == 4,
            'Password level 4')
    test_condition(check_password_level(' badpassword') == -1,
            'Invalid password')
    test_condition(check_password_level('\rbadpassword') == -1,
            'Invalid password')
    test_condition(check_password_level('\vbadpassword') == -1,
            'Invalid password')
    test_condition(check_password_level('\fbadpassword') == -1,
            'Invalid password')
    test_condition(check_password_level('\tbadpassword') == -1,
            'Invalid password')
    test_condition(check_password_level('\nbadpassword') == -1,
            'Invalid password')
    print()
    print('Generated passwords')
    password = generate_password(5, 1)
    message = ' (' + password + ')'
    test_condition(check_password_level(password) == 1,
            'Password level 1' + message)
    password = generate_password(8, 1)
    message = ' (' + password + ')'
    test_condition(check_password_level(password) == 2,
        'Password level 2 (long)'  + message)
    password = generate_password(5, 2)
    message = ' (' + password + ')'
    test_condition(check_password_level(password) == 2,
        'Password level 2 (short)' + message)
    password = generate_password(8, 2)
    message = ' (' + password + ')'
    test_condition(check_password_level(password) == 3,
        'Password level 3 (long)' + message)
    password = generate_password(5, 3)
    message = ' (' + password + ')'
    test_condition(check_password_level(password) == 3,
        'Password level 3 (short)' + message)
    password = generate_password(5, 4)
    message = ' (' + password + ')'
    test_condition(check_password_level(password) == 4,
        'Password level 4' + message)
    print()


