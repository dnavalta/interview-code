""" TITLE: test_database.py 
    AUTHOR: Dalton Navalta
    DATE: 15 August, 2018
    
    Tests the functions in navalta_interview from interview-code repository.
    Answers item 5 in Python challenge.
"""

from random import choice  # https://docs.python.org/3.6/library/random.html
from navalta_interview import create_user, generate_password


if __name__ == '__main__':
    # Initialize lists of valid lengths and complexities
    lengths = list(range(6,13))
    complexities = list(range(1,5))

    # Ten times
    for _ in range(10):
        # Create user in user_data.db
        create_user('user_data.db',
                # Using randomly generated password of random lenght and
                #   complexity
                generate_password(choice(lengths), choice(complexities)))
    # create_user('no_database.db',
    #         generate_password(choice(lengths), choice(complexities)))


